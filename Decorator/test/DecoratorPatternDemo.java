package test;
import decorators.RedShapeDecorator;
import decorators.TransparencyDecorator;
import shapes.Circle;
import shapes.Rectangle;
import shapes.IShape;

public class DecoratorPatternDemo {
	public static void main(String[] args) {
		
		IShape circle    = new Circle();
		IShape redRect   = new RedShapeDecorator(new Rectangle());
		IShape redCircle = new RedShapeDecorator(new Circle());
		IShape redTranspCircle = new RedShapeDecorator(new TransparencyDecorator(new Circle(), (byte) 45));
		IShape transpRedCircle = new TransparencyDecorator(new RedShapeDecorator(new Circle()), (byte) 55);
		
		circle.draw();
		redRect.draw();
		redCircle.draw();
		redTranspCircle.draw();
		transpRedCircle.draw();
	}
}
