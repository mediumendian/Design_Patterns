package decorators;

import shapes.IShape;

public class TransparencyDecorator extends ShapeDecorator {
	
	public IShape shape;
	public byte  transp;
	
	public TransparencyDecorator(IShape decoratedShape, byte transp) {
		super(decoratedShape);
		this.transp = transp;
	}

	@Override
	public void draw() {
		decoratedShape.draw();
		setTransparency();
	}

	private void setTransparency() {
		System.out.println("Transparency: " + transp);
	}

}
