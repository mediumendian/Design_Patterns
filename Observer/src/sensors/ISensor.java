package sensors;

import listener.IListener;

public interface ISensor {
	
	public void register(IListener obj);
	public void unregister(IListener obj);
	
	public void notifyObservers();
	
	public Object getUpdate(IListener obj);

}
