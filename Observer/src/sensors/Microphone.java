package sensors;

import java.util.ArrayList;
import java.util.List;

import listener.IListener;

public class Microphone implements ISensor {

	private List<IListener> listeners;
	private boolean changed;
	private final Object MUTEX = new Object();
	private double value;

	public Microphone() {
		this.listeners = new ArrayList<>();
	}

	@Override
	public void register(IListener obj) {
		if (obj == null) {
			throw new NullPointerException("Null Observer");
		} else {
			synchronized (MUTEX) {
				if (!listeners.contains(obj)) {
					listeners.add(obj);
				}
			}
		}
	}

	@Override
	public void unregister(IListener obj) {
		synchronized (MUTEX) {
			listeners.remove(obj);
		}
	}

	@Override
	public void notifyObservers() {
		List<IListener> listenersLocal = null;
		// with synchronization so no listeners are registered during operation
		synchronized (MUTEX) {
			if (!changed) {
				return;
			}
			listenersLocal = new ArrayList<>(this.listeners);
			this.changed = false;
		}
		for (IListener obj : listeners) {
			obj.update();
		}
	}

	@Override
	public Object getUpdate(IListener obj) {
		return value;
	}
	
	public void logValue(double newVal) {
		System.out.println("Value recorded: " + value);
		this.value = newVal;
		this.changed = true;
		notifyObservers();
	}

}
