package listener;

import sensors.ISensor;

public interface IListener {
	
	public void update();
	public void setSubject(ISensor sub);

}
