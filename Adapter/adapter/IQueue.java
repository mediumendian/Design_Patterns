package adapter;

import java.util.Optional;

public interface IQueue {
	public boolean isEmpty();
	public int getSize();
	public void offer(int element);
	public int peek();
	public Optional<Integer> poll();
}
