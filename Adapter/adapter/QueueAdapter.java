package adapter;

import java.util.Optional;

import circularBuffer.CircularBuffer;

public class QueueAdapter implements IQueue {

	private CircularBuffer rp;
	
	public QueueAdapter(int capacity) {
		rp = new CircularBuffer(capacity);
	}

	@Override
	public boolean isEmpty() {
		return rp.isEmpty();
	}

	@Override
	public int getSize() {
		return rp.getNumOfElems();
	}

	@Override
	public void offer(int element) {
		rp.offer(element);
	}

	@Override
	public int peek() {
		return rp.peek();
	}

	@Override
	public Optional<Integer> poll() {
		return rp.poll();
	}

}
