package circularBuffer;

import java.util.stream.IntStream;

public class TestCircularBuffer {

	public static void main(String[] args) {
		int j;
		CircularBuffer cb = new CircularBuffer(5);
		
		IntStream.range(0, 20).forEach(i -> cb.offer(i));
		IntStream.range(0, 10).forEach(i -> System.out.println(cb.poll()));
		IntStream.range(0, 10).forEach(i -> cb.offer(i));

	}
}

/*
 * offered 16 0, 1, 2, 3, 4, polled 5 offered 25 5, 6, 7, 8, 9, 10, 11, 12, 13,
 * 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
 * polled 30
 */
